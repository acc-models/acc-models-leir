system, "mkdir output";

/**********************************************************************************
*
* MAD-X input script for the flat bottom optics of the NOMINAL cycle.
* 22/05/2020 - Alexander Huschauer
************************************************************************************/

/******************************************************************
 * Energy and particle type definition
 ******************************************************************/

call, file = "leir_fb_nominal.beam";
BRHO := BEAM->PC * 3.3356 * (208./54.);

/******************************************************************
 * Call lattice files
 ******************************************************************/

call, file = "leir.seq";
call, file = "leir_fb_nominal.str";
call, file = "leir.dbx";
call, file = "macros.madx";

/**********************************************************************************
 *                   Global coupling correction and matching
***********************************************************************************/
TuneH  =  1.82;
TuneV  =  2.72;

!Optical parameters at the centre of the electron cooler.
DisEC  = -0.00;
BetHEC =  5.00;
BetVEC =  5.00;

use, sequence=LEIR;
exec, global_correction;
exec, write_str_file('./output/leir_fb_nominal.str');

/**********************************************************************************
 * PTC Twiss
***********************************************************************************/

! obtain smooth optics functions by slicing the elements
use, sequence=LEIR;
exec, ptc_twiss_macro(2,0,1);
exec, write_ptc_twiss("./output/leir_fb_nominal.tfs");
